package com.vino.webapp.Configuration;

import javax.ws.rs.core.Application;
import javax.ws.rs.ApplicationPath;


@ApplicationPath("test/")
public class Config extends Application {
}
