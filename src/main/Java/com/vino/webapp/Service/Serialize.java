package com.vino.webapp.Service;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


public class Serialize {
    private static Document convertStringToXMLDocument(String xmlString) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();

            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } catch (Exception e) {

        }
        return null;
    }

    public boolean getXml(String xml) throws Exception {
        final String xmlStr = xml;
        Document document = convertStringToXMLDocument(xmlStr);

        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        Source schemaFile = new StreamSource(new File("/Users/vinojiniparamasivam/WebAppJava/src/resources/customer.xsd"));
        Schema schema = factory.newSchema(schemaFile);


        Validator validator = schema.newValidator();


        try {
            validator.validate(new DOMSource(document));
            return true;
        } catch (SAXException e) {
            return false;

        } catch (IOException e) {
            return false;
        }

    }
}