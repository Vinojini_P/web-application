package com.vino.webapp.DAO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="customer")
public class Customer {

    @Id
    @Column(name="id")
    private int id;

    @Id
    @Column(name="firstName")
    private String firstName;


    public Customer(int id, String firstName) {
        this.id = id;
        this.firstName = firstName;
    }

    public Customer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String toString() {
        return "Id: " + this.id + "\n" + "First Name: " + this.firstName;
    }
}
