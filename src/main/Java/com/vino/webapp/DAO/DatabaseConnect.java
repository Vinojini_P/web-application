package com.vino.webapp.DAO;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;


public class DatabaseConnect {
    static final String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/Customer";

    static final String USER = "root";
    static final String PASS_WORD = "";

    private static GenericObjectPool genericPool;

    @SuppressWarnings("unused")
    public DataSource initialPool() throws Exception {
        Class.forName(DB_DRIVER);

        genericPool = new GenericObjectPool();
        genericPool.setMaxActive(15);

        ConnectionFactory conFac = new DriverManagerConnectionFactory(DB_URL, USER, PASS_WORD);

        PoolableConnectionFactory pcf = new PoolableConnectionFactory(conFac, genericPool, null, null, false, true);
        return new PoolingDataSource(genericPool);
    }

    public static void addToDb(Customer customer) {
        ResultSet resultSet = null;
        Connection connection = null;
        PreparedStatement prepState = null;
        DatabaseConnect DataCon = new DatabaseConnect();


        try {
            DataSource dataSource = DataCon.initialPool();
            connection = dataSource.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Customer.Customer (id ,firstName) VALUES (? , NULL )");
            preparedStatement.setInt(1, customer.getId());
//            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.executeUpdate();

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (prepState != null) {
                    prepState.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
            }
        }
    }
}