package com.vino.webapp.Controller;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.vino.webapp.DAO.Customer;
import com.vino.webapp.DAO.DatabaseConnect;
import com.vino.webapp.Service.Serialize;
import com.vino.webapp.Service.Validate;
import org.xml.sax.SAXException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;


@Path("/customer")
public class Controller {
    Validate valid = new Validate();
    Serialize serialize = new Serialize();

    @Path("/validate")
    @POST
    @Consumes("application/xml")
    @Produces("application/xml")
    public Response getpersonDetails(String data) throws SAXException {
        if (valid.valid(data)) {
            return Response.status(200).entity("Valid Data").build();
        } else {
            return Response.status(400).entity("Invalid Data").build();
        }

    }

    @Path("/serialize")
    @POST
    @Consumes("application/xml")
    @Produces("application/json")
    public Response serialize(String data) {
        try {
            if (serialize.getXml(data)) {
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                XmlMapper xmlMapper = new XmlMapper();
                xmlMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                Customer cus = xmlMapper.readValue(data,Customer.class);
                DatabaseConnect.addToDb(cus);
                return Response.status(200).entity("Valid Data").build();
            } else {
                return Response.status(400).entity("Invalid Data").build();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok().build();


    }
}
